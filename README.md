# Frontend Mentor - NFT preview card component solution

This is a solution to the [NFT preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/nft-preview-card-component-SbdUL_w0U). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Frontend Mentor - NFT preview card component solution](#frontend-mentor---nft-preview-card-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Continued development](#continued-development)
  - [Author](#author)
  - [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

-   View the optimal layout depending on their device's screen size
-   See hover states for interactive elements

### Screenshot

-   Desktop design: ![desktop](/public/images/desktop-design.png)
-   Mobile design: ![mobile](public/images/mobile-design.png)

### Links

-   Solution URL: [Gitlab](https://gitlab.com/xiayudev/nft-preview-card)
-   Live Site URL: [Surge](https://abrupt-scale.surge.sh/)

## My process

### Built with

-   Semantic HTML5 markup
-   CSS custom properties
-   Flexbox
-   CSS Grid
-   Mobile-first workflow
-   [Tailwind CSS](https://tailwindcss.com/) - CSS Framework
-   [Astro](https://astro.build/) - Static Site Generator

### What I learned

Tailwind CSS is a great framework, easy to learn and apply. Also, Astro a great site generator. If we practice it more and more, we can master any framework or language.

### Continued development

A little tricky about tailwind I found is pseudo-classes, to be more precise, ::after. I have to practice it a lot.

## Author

-   Website - [xiayudev](https://xiayudevsportfoliov2.netlify.app/)
-   Frontend Mentor - [@TheSunLand7](https://www.frontendmentor.io/profile/TheSunLand7)
-   Twitter - [@J7Jeo](https://twitter.com/J7Jeo)

## Acknowledgments

For icons, use pseudo-class ::before or ::after. I hope this tip can help you.
